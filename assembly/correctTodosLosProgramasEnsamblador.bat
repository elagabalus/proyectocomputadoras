@echo off
FOR %%A in (1 3 4) DO CALL :compileRun %%A
echo TODOS LOS PROGRAMAS FUERON EJECUTADOS
goto TheEnd

:compileRun
echo COMPILANDO PROGRAMA %~1
\MASM32\BIN\Ml.exe /c EJ00CA0%~1.asm
if errorlevel 1 goto errasm

:nores
\MASM32\BIN\link.exe  EJ00CA0%~1.obj
if errorlevel 1 goto errlink
echo CORRIENDO PROGRAMA %~1
EJ00CA0%~1
pause
EXIT /B 0

:errlink
echo "Hubo un error"
echo _
echo Link error

goto TheEnd

:errasm
echo "Hubo un error"
echo _
echo Assembly Error
goto TheEnd

:TheEnd
pause
