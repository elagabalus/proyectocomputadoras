/* Elaborado por :
Santander Martínez Ángel Antonio*/
/* Fecha de elaboración 
 10 noviembre 2019 */
/* Programa para calcular el factorial de un numero */
/* Descripcion del programa */
/* Este programa calcula el factorial de un numero 
dicho se introduce por medio del telcado, a su vez 
se realizan las validaciones correspondientes */
#include<stdio.h> /* Acceso a archivo de biblioteca */
#include<stdlib.h> /* Acceso a archivo de biblioteca */
#include<ctype.h> /* Acceso a archivo de biblioteca */
#include<string.h> /* Acceso a archivo de biblioteca */
#include<time.h> /* Se incluyen la biblioteca para medir el tiempo de ejecución */
int main(void) /* Cabecera de funcion */
{
    /* Declaracion de variables */
    time_t prin,fin; /* Tiempos de comienzo y terminacion*/
    /* Numero del factorial */
    int num_factorial=-1; /* Numero factorial */
    register int num_factorial_aux=0,/* Numero factorial auxiliar */
    calculo_factorial=0, /* Calculo de facotorial */
    ciclo;
    /* Declaracion de funciones */

    /* Funcion de calculo del factorial */
    int factorial(int num_factorial);
    /* inicio de sentencias de programacion */
    /* validacion del factorial */
    while(num_factorial<0){
        printf("Diga el numero para el factorial = ?\n");
        scanf("%d",&num_factorial);
        // system("PAUSE");
    }
    /* Sentencia de asignacion y llamada de función*/
    num_factorial_aux=num_factorial;
    // printf("El valor de num_factorial %d \n", num_factorial);
    if ((num_factorial==0)||(num_factorial==1))
    {
        num_factorial=1;
        printf("El valor de aux_factorial %d \n",num_factorial);
        calculo_factorial=1;
    }
    else
    {
        /*Marcar el tiempo de comienzo */
        time(&prin);
        // printf("El valor de num_factorial %d \n",num_factorial);
        for(ciclo=1; ciclo<=999999999;++ciclo){
            calculo_factorial=factorial(num_factorial);
        }
        /* Ajustar el contador y marcar el tiempo de terminacion */
        time(&fin);
    }
    /*Impresion de resultados*/
    printf("El factorial del numero %d es %d\n",num_factorial_aux,calculo_factorial);
    printf("Tiempo transcurrido: %.03f segundos\n",difftime(fin,prin));
    system("PAUSE");
    return 0;
}
/* Funcion factorial */
int factorial(int aux_factorial)
{
    /* Declaracion de variables */
    register int contador_aux=0, /* Contador que decrementa*/
    aux_fact=0; /* Variable del calculo de factorial */
    /* Sentencias de la funcion */
    contador_aux=aux_factorial;
    aux_fact=aux_factorial;
    for(contador_aux=aux_factorial;contador_aux>=2;--contador_aux){
        aux_fact=aux_fact*(contador_aux-1);
        /* printf("El valor del contador %d y el valor de aux_fact %d\n",
        contador_aux,aux_fact)*/
    }
    // printf("Valor encontrado %d\n",aux_fact);
    /* Fin de funcion*/
    return (aux_fact);
}