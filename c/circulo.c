/* Elaborado por :
Santander Martínez Ángel Antonio*/
/* Fecha de elaboración 
 10 noviembre 2019 */
/* Programa para calcular el area un circulo */
/* DESCRIPCION DEL PROGRAMA */
/* Este programa calcula el area de un circul,
los datos se introducen por medio de teclado */
#include <stdio.h> /* Acceso a archivo de la biblioteca */
#include <stdlib.h> /* Acceso a archivo de la biblioteca */
#include <time.h> /* Se incluyen la biblioteca para medir el tiempo de ejecución */
#define PI 3.141526 /* Declaracion de variable global */
int main(int argc, char const *argv[]) /* cabecera de función */
{
    /* Declaracion de variables */
    time_t prin,fin; /* Tiempos de comienzo y terminacion*/
    float radio, /* Radio de circulo */
    area; /* Área del circulo */
    int contador,ciclo; /* Contador auxiliar */
    int total_circulos=0; /* Total circulos */
    /* declaracion de funciones */
    float procesar(float radio); /* Funcion de calculo de area */

    /* Inicio de sentencias de programacion */

    /* Validacion para total de circulos */
    while(total_circulos<=0){
        printf("Cuantos circulos\n");
        scanf("%d",&total_circulos);
    }
    /* sentencia de entrada y salida contador de todal de circulos */
    for(contador=1;contador<=total_circulos;contador++){
        printf("Circulo No %d: Radio = ?\n",contador);
        scanf("%f",&radio);
        /* valiacion de datos */
        if(radio<0)
            area=0;
        else
            /*Marcar el tiempo de comienzo */
            time(&prin);
            /* sentencia de asignacion y llamada de funcion */
            for(ciclo=1; ciclo<=999999999;++ciclo)
            {
                area=procesar(radio);
            }
            /* marcar el tiempo de terminacion */
            time(&fin);
        /* Impresion de resultados */
        printf("Area = %f\n",area); /* sentencia de salida */
        printf("Tiempo transcurrido: %.01f segundos\n",difftime(fin,prin));
    }
    system("PAUSE"); /* Sentencia de sistema para indicar continuacion */
    return 0;
}
/* Funcion procesar */
float procesar(float rad)
{
    /* declaracion de variables */
    float are;
    /* sentencia de funcion */
    are=PI*rad*rad;
    /* Fin de funcion */
    return (are);
}

