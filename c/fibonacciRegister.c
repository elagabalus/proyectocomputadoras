﻿/* Elaborado por :
Santander Martínez Ángel Antonio*/
/* Fecha de elaboración 
 10 noviembre 2019 */
#include<stdio.h> /* Se incluye la bibloteca de entrada y salida */
#include<time.h> /* Se incluyen la biblioteca para medir el tiempo de ejecución */
/* Calcular 30,000,000 vecees los primeros 23 numeros de la serie fibonacci
para ilustrar el uso de las varaibles registro */
int main()
{
    time_t prin,fin; /* Tiempos de comienzo y terminacion*/
    register int f, f1, f2;
    register int cont, ciclo,n=23;
    /*Marcar el tiempo de comienzo */
    time(&prin);
    /* Hacer 30, 000 ciclos*/
    for(ciclo=1; ciclo<=99999999;++ciclo){
        f1=1;
        f2=1;
        /*Generar los primero numeros de fibonacci*/
        for(cont=1;cont<=n;++cont){
            f=(cont<3)?1:f1+f2;
            f2=f1;
            f1=f;
        }
    }
    /* Ajustar el contador y marcar el tiempo de terminacion */
    --cont;
    time(&fin);
    /* Mostrar la saldia */
    printf("i= %2d  F=%d\n",cont,f);
    printf("Tiempo transcurrido: %.02f segundos\n",difftime(fin,prin));
    system("PAUSE");
    return 0;
}
